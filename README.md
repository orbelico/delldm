# delldm

A simple replacement for Dell display manager on Linux.

## WARNING

**This tool comes without any warranty whatsoever. It is tested with one single monitor only - use at your own risk!
Before you start using this tool you need to properly configure it. See below for details.**

## Prerequisites

delldm is simply a bash-script wrapper around [ddcutil](https://www.ddcutil.com/). As such, ddcutil needs to be 
installed, e.g. on debian-based systems, do `apt install ddcutil`.

ddcutil communicates with the display via I2C bus. In order to use it without root permissions, you will need to add
your user to the i2c group (again, on debian-based systems, YMMV on other distros).

Once installed, run `ddcutil detect` and look for the dell monitor's entry. You will find something like  
`I2C bus:  /dev/i2c-4`  
In this example, the 4 is the ID of the relevant I2C bus.

Then, run `ddcutil -b $BUS_ID capabilities`, where `$BUS_ID` is the number you just identified. It will show you the
different functions of your display. Take note of the addresses for input selection and the picture-in-picture
capability.

## Installation and Configuration

Clone this git repository to a folder of your choice. Do **not** start the `delldm` script yet, instead open it for 
editing.

Make the following adjustments:

* Edit the `I2C_BUS_ID` variable at the beginning of the script to match your I2C bus id.
* Look through the script for all calls to ddcutil. These contain addresses in hex format at the end, e.g.: `0x60 0x1b`.
  These work with **my Dell U3421WE** display. For your display, the addresses may be identical, or totally different 
  (maybe even if you own the same model).
  From what you've learned from the output of ddcutil's `capabilities` mode (see above), try to adjust them to the best
  of your knowledge. **I am not responsible for any incorrect addresses you set here and take no lieability for any 
  damage this tool might do to your hardware.** If you have trouble understanding what the code does and how you need
  to adjust it, do not use this tool.

In order to have the tool available without switching to the git directory first, do the following:

* Make the file executable: `chmod +x delldm`
* Create a symbolic link to it in a folder which is on your path, like `~/bin` or `/usr/local/bin`
* Optional, but reommended: bind typcial calls to delldm, like selecting your regularly used input sources, to hotkey
  combinations. This can usually be done easily through the settings of your desktop environment.

## Usage

delldm has two modes, one to set the active input, another one to enable/disable picture-by-picture mode.

Use `delldm input $INPUT`, where `$INPUT` is one of `usbc, hdmi1, hdmi2, dp` to select the respective video source.

Use `delldm pbp $CMD`, where `$CMD` can be one of `on, off` to enable/disable PBP mode, `toggle` to switch between 
enabled/disabled PBP, and `status` to query if PBP is currently enabled.

There is currently no support for different PBP variants, like different percentages of split-screen size. The command
in the code sets 50%/50% mode for my monitor model (see above), but it may be different for yours, or you may decide to
choose a different split ratio by defining the command differently.

## Contributing

If this tool works with your monitor model, let me know by creating an issue, and I will document it. Also, feel free to
open PRs for extending the functionality. I am aware that currently it is quite limited, but at the moment, this is all
I need.
